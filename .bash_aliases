#!/bin/bash
alias ù='%'

alias burp='burpsuite &> /dev/null & disown %'
alias cq='cd ..'

alias dirbuster='nohup dirbuster >/dev/null 2>&1 &'
alias du='du -h'

alias ebrc='vim ~/.bashrc'
alias eba='vim  ~/.bash_aliases'

alias firefox='firefox &> /dev/null & disown %'

alias grep='grep --color=auto -n'

alias irc='ssh reynaud2012@perso.iiens.net'
alias jobs='jobs -l'

alias ls='ls --color=auto --group-directories-first'
alias la='ls -A'

alias pattern_create='/usr/share/metasploit-framework/tools/exploit/pattern_create.rb'
alias pattern_offset='/usr/share/metasploit-framework/tools/exploit/pattern_offset.rb'
alias python='python3'

alias sba='. ~/.bash_aliases'
alias sbrc='. ~/.bashrc'
